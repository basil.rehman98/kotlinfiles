 data class Students(val name: String,val id:Int)

  fun daTa(){
     val firststudent=Students("Ibrahim",10)
     val secondstudent=Students("Ishaq",20)
     val thirdstudent=Students("Ismail",30)   //data classes in which equals intances has equal hascode
     println(firststudent.hashCode())                  //it has copy property which is used to modify values and
      println(secondstudent.copy(id=200))              //auto-gen component function to get values of any intances
}
 data class Teacher(val name: String,val id: Int)
 fun data(){
     val firstteacher=Teacher("Ibrahim",10)
     val secondteacher=Teacher("Ishaq",20)
     val thirdteacher=Teacher("Ismail",30)
     println(firstteacher.hashCode())
     println(thirdteacher.component1())
     println(secondteacher.component2())
 }