sealed class School(val name: String)
    class students(val stuname: String, val rollno:Int): School(stuname)
    class teachers(val teaname:String, val designation:String):School(teaname)
    fun startschool(school:School):String{
        when(school){
            is students->return "hello! dear ${school.stuname} and your roll no is:${school.rollno}"
            is teachers->return "have a nice day:${school.teaname} and your designation is:${school.designation}"
        }
    }
