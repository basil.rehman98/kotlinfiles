 open class Profile(){
    open fun user(name:String, phonenumber: Long, email:String, operation:(String,Long, String)->String):String{

        return operation(name,phonenumber,email)
    }
    //open fun profile (name: String, phonenumber: Int,email: String){name+phonenumber+email}
}


 data class Item(val name: String, val price: Float)                                   // 1

 data class Order(val items: Collection<Item>)

 fun Order.maxPricedItemValue(): Float = this.items.maxBy { it.price }?.price ?: 0F    // 2
 fun Order.maxPricedItemName() = this.items.maxBy { it.price }?.name ?: "NO_PRODUCTS"

 val Order.commaDelimitedItemNames: String                                             // 3
     get() = items.map { it.name }.joinToString()
