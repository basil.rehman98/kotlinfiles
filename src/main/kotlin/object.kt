open class Rentoffarm {
    open fun Paying(weekdays: Int, weekends: Int, favourabledays: Int) {
        val rates = object {
            var Weekdays: Int = 10 * weekdays
            var Weekends: Int = 30 * weekends
            var Favourabledays: Int = 50 * favourabledays
        }
        val total = rates.Weekdays + rates.Weekends + rates.Favourabledays
        println("total rent= $$total")
    }
}