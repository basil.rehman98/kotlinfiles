class Nmap {

    val numbers = listOf(10, -20, 30, -40, 50,-60)
    val teentype = numbers.map { it * 3 }
}
const val POINTS_X_PASS: Int = 15
class MMap{
    val EZPassAccounts: MutableMap<Int, Int> = mutableMapOf(1 to 100, 2 to 100, 3 to 100)   // 1
    val EZPassReport: Map<Int, Int> = EZPassAccounts                                        // 2

    fun updatePointsCredit(accountId: Int) {
        if (EZPassAccounts.containsKey(accountId)) {                                        // 3
            println("Updating $accountId...")
            EZPassAccounts[accountId] = EZPassAccounts.getValue(accountId) + POINTS_X_PASS  // 4
        } else {
            println("Error: Trying to update a non-existing account (id: $accountId)")
        }
    }

    fun accountsReport() {
        println("EZ-Pass report:")
        EZPassReport.forEach {                                                              // 5
            k, v -> println("ID $k: credit $v")
        }
    }
}