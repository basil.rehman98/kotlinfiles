enum class Color(val rgb:Int){
RED(0xFF0000),
GREEN(0x00FF00),
BLUE(0x0000FF),
yellow(0xFFFF00),
GOLD(0xFFD700);

    fun containsRed()= (this.rgb and 0xFF0000 !=0)

}
enum class State {              //enum class is use to define state or modes of intance
    idle, running, finish;

    fun status() {
        val state = State.idle
        val message = when (state) {
            State.finish -> "it is Done"
            State.idle -> "it is Idle"
            State.running -> "it is Running"
        }
        println(message)
    }
}