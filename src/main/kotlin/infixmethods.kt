infix fun Int.times(str: String) = str.repeat(this)        // 1
                                    // 2

val pair = "Ferrari" to "Katrina"                          // 3


infix fun String.onto(other: String) = Pair(this, other)   // 4
val myPair = "McLaren" onto "Lucas"


val sophia = Person("Sophia")
val claudia = Person("Claudia")

class Person(val name: String) {
    val likedPeople = mutableListOf<Person>()
    infix fun likes(other: Person) { likedPeople.add(other) }  // 6
}
/*methods*/